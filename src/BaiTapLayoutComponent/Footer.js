import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div
        className="bg-dark text-light"
        style={{ height: "8rem", width: "100%", position: "relative" }}
      >
        <p
          style={{
            position: "absolute",
            bottom: "35%",
            left: "50%",
            transform: "translateX(-50%)",
          }}
        >
          Copyright © Your Website 2022
        </p>
      </div>
    );
  }
}
